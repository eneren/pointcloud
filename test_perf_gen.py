import os
import time
import math
import argparse
import torch
from tqdm.auto import tqdm

from utils.dataset import *
from utils.misc import *
from utils.data import *
from models.vae_gaussian import *
from models.vae_flow import *
from models.flow import add_spectral_norm, spectral_norm_power_iteration
from evaluation import *



# Arguments
parser = argparse.ArgumentParser()
parser.add_argument('--ckpt', type=str, default='/home/jovyan')
parser.add_argument('--save_dir', type=str, default='/eos/user/e/eneren/point_cloud/')
parser.add_argument('--device', type=str, default='cuda')
parser.add_argument('--categories', type=str_list, default=['uniform10-100GeV'])

# Sampling
parser.add_argument('--batch_size', type=int, default=100)
parser.add_argument('--nshowers', type=int, default=4000)
parser.add_argument('--sample_num_points', type=int, default=4000)
parser.add_argument('--seed', type=int, default=9988)
args = parser.parse_args()


# Logging
save_dir = os.path.join(args.save_dir, 'GEN_4D_Timing_%s_%d' % ('_'.join(args.categories), int(time.time())) )
if not os.path.exists(save_dir):
    os.makedirs(save_dir)
logger = get_logger('test_perf', save_dir)
for k, v in vars(args).items():
    logger.info('[ARGS::%s] %s' % (k, repr(v)))

# Checkpoint
ckpt = torch.load(args.ckpt)
seed_all(args.seed)

# Datasets and loaders
logger.info('Loading datasets...')


# Model
logger.info('Loading model...')
if ckpt['args'].model == 'gaussian':
    model = GaussianVAE(ckpt['args']).to(args.device)
elif ckpt['args'].model == 'flow':
    model = FlowVAE(ckpt['args']).to(args.device)
logger.info(repr(model))
# if ckpt['args'].spectral_norm:
#     add_spectral_norm(model, logger=logger)
model.load_state_dict(ckpt['state_dict'])


start = torch.cuda.Event(enable_timing=True)
end = torch.cuda.Event(enable_timing=True)

# Generate Point Clouds
gen_pcs = []
time_pcs = []
for i in range(0, args.nshowers, args.batch_size):
    with torch.no_grad():
        start.record() 
        z = torch.randn([args.batch_size, ckpt['args'].latent_dim]).to(args.device)
        x = model.sample(z, args.sample_num_points, flexibility=ckpt['args'].flexibility)
        end.record()
        torch.cuda.synchronize()
        logger.info("Time to generate %d showers: %d " % (args.batch_size,  start.elapsed_time(end)) )
        time_pcs.append(start.elapsed_time(end))
        gen_pcs.append(x.detach().cpu())

gen_pcs = torch.cat(gen_pcs, dim=0)[:2000]
logger.info("average time per shower is %d ms" % (sum(time_pcs) / len(time_pcs) / (args.batch_size)))



# Save
logger.info('Saving point clouds...')
np.save(os.path.join(save_dir, 'out.npy'), gen_pcs.numpy())


